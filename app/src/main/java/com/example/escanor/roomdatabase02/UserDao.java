package com.example.escanor.roomdatabase02;

import android.net.Uri;

import androidx.room.Dao;
import androidx.room.Insert;

@Dao
public interface UserDao {
    @Insert
    public void insertUser(User user);
}
